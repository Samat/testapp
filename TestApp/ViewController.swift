//
//  ViewController.swift
//  TestApp
//
//  Created by Zhexenov Samat on 8/13/17.
//  Copyright © 2017 Zhexenov Samat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    let errorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = UIColor.red
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var idnTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "IDN"
//        tf.text = "120000000004"
        tf.delegate = self
        tf.tag = 1
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        return tf
    }()
    
    lazy var passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Password"
//        tf.text = "12345678"
        tf.isSecureTextEntry = true
        tf.tag = 2
        tf.delegate = self
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        return tf
    }()
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    let indicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.activityIndicatorViewStyle = .whiteLarge
        view.color = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.hidesWhenStopped = true
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupInputFields()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setupInputFields() {
        let stackView = UIStackView(arrangedSubviews: [idnTextField, passwordTextField, signUpButton])
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        
        view.addSubview(stackView)
        view.addSubview(errorLabel)
        
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40),
            stackView.heightAnchor.constraint(equalToConstant: 200)
            ])
        
        view.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 20).isActive = true
        signUpButton.addTarget(self, action: #selector(signIn), for: .touchUpInside)
        errorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30).isActive = true
        errorLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        errorLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let backspaceTapped = range.length == 1 && string.count == 0
        let text = textField.text ?? ""
        let decimalDigitSet = CharacterSet.decimalDigits
        let characterSet = CharacterSet.alphanumerics
        textField.removeError()
        
        if textField.tag == 1 {
            if string.rangeOfCharacter(from: decimalDigitSet.inverted) != nil || (text.count > 11 && !backspaceTapped) {
                return false
            }
        } else if textField.tag == 2 {
            if (string.rangeOfCharacter(from: decimalDigitSet.inverted) != nil &&
                string.rangeOfCharacter(from: characterSet.inverted) != nil) || (text.count > 11 && !backspaceTapped) {
                return false
            }
        }
        return true
    }
    
    @objc func signIn() {
        let idn = idnTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        var bool = true
        
        if idn.count != 12 {
            idnTextField.setError()
            bool = false
        }
        
        if password.count < 5 {
            passwordTextField.setError()
            bool = false
        }
        
        if !bool {
            return
        }
        indicator.startAnimating()
        errorLabel.text = ""
        
        RequestService.signIn(idn: idn, password: password, completionHandler: {
            response in
            self.indicator.stopAnimating()
            var errorMsg = "Ошибка авторизации"
            if let model = response {
                if !(model.status ?? false) {
                    if let arr = model.errors {
                        for msg in arr {
                            errorMsg = msg
                        }
                    }
                    self.errorLabel.text = errorMsg
                } else {
                    if let number = model.content?.call_center {
                        let vc = CallController()
                        vc.number = number
                        let navController = UINavigationController(rootViewController: vc)
                        self.present(navController, animated: true, completion: nil)
                    } else {
                        self.errorLabel.text = "Номер call-centre не найдено"
                    }
                    
                }
            } else {
                self.errorLabel.text = errorMsg
            }
        })
        
    }


}























