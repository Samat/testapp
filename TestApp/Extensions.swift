//
//  Extensions.swift
//  TestApp
//
//  Created by Zhexenov Samat on 8/14/17.
//  Copyright © 2017 Zhexenov Samat. All rights reserved.
//

import UIKit

extension UITextField {
    func setError() {
        layer.masksToBounds = true
        layer.cornerRadius = 3
        layer.borderWidth = 1
        layer.borderColor = UIColor.red.cgColor
        if let placeholder = placeholder {
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
        }
        textColor = UIColor.red
    }
    
    func removeError() {
        layer.masksToBounds = false
        layer.borderWidth = 0
        if let placeholder = placeholder {
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 0.78, green: 0.78, blue: 0.80, alpha: 1.0)])
        }
        textColor = UIColor.black
    }
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}


