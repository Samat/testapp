//
//  Model.swift
//  TestApp
//
//  Created by Zhexenov Samat on 8/17/17.
//  Copyright © 2017 Zhexenov Samat. All rights reserved.
//

import Foundation

struct Model: Codable {
    var status: Bool?
    var content: Content?
    var message: String?
    var errors: [String]?
}

struct Content: Codable {
    var call_center: String?
    var id: Int?
}

struct CallModel: Codable {
    var status: Bool?
    var content: CallContent?
}

struct CallContent: Codable {
    var id: Int?
}
