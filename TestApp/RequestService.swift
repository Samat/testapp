//
//  RequestService.swift
//  TestApp
//
//  Created by Zhexenov Samat on 8/13/17.
//  Copyright © 2017 Zhexenov Samat. All rights reserved.
//

import UIKit
import Alamofire

class RequestService {
    
    fileprivate static let baseUrl = "http://pc-staging.zlife.kz/api/v1/"
    static var token = "", uid = "", client = ""
    
    enum ResourcePath: CustomStringConvertible {
        case signIn()
        case call()
        case putCall(id: Int)
        
        var description: String {
            switch self {
            case .signIn():
                return "patient/sign_in"
            case .call():
                return "patient/calls/start"
            case .putCall(let id):
                return "patient/calls/\(id)/end"
            }
            
        }
    }
    
    static func signIn(idn: String, password: String, completionHandler: @escaping (Model?) -> ()) {
        let urlString = baseUrl + ResourcePath.signIn().description
        guard let url = URL(string: urlString) else {
            completionHandler(nil)
            return
        }
        
        let params: Parameters = ["iin": idn, "password": password]
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default).responseData(completionHandler: {
            response in
            if let headers = response.response?.allHeaderFields as? [String: String] {
                if let token = headers["Access-Token"] {
                    self.token = token
//                    print(token)
                }
                if let uid = headers["Uid"] {
                    self.uid = uid
//                    print(uid)
                }
                if let client = headers["Client"] {
                    self.client = client
//                    print(client)
                }
            }
            
            if let data = response.result.value {
                do {
                    let model = try JSONDecoder().decode(Model.self, from: data)
                    completionHandler(model)
                } catch let error {
                    completionHandler(nil)
                    print(error.localizedDescription)
                }
            } else {
                completionHandler(nil)
            }
            
        })
        
    }
    
    static func call(completionHandler: @escaping(CallModel?) -> ()) {
        let urlString = baseUrl + ResourcePath.call().description
        guard let url = URL(string: urlString) else {
            completionHandler(nil)
            return
        }
        let headers: HTTPHeaders = ["Access-Token": token,
                                  "Uid": uid,
                                  "Client": client]

        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData(completionHandler: { response in
            
            if let data = response.result.value {
                do {
                    let model = try JSONDecoder().decode(CallModel.self, from: data)
                    completionHandler(model)
                } catch let error {
                    completionHandler(nil)
                    print(error.localizedDescription)
                }
            } else {
                completionHandler(nil)
            }
        })
    }
    
    static func putCall(id: Int, duration: Int, completionHandler: @escaping(Bool) -> ()) {
        let urlString = baseUrl + ResourcePath.putCall(id: id).description
        guard let url = URL(string: urlString) else {
            completionHandler(false)
            return
        }
        
        let os = UIDevice.current.systemVersion
        print(os)
        let params: Parameters = ["os": "ios", "call_duration": duration, "os_version": os]
        let headers: HTTPHeaders = ["Access-Token": token,
                                    "Uid": uid,
                                    "Client": client]
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            
            if let data = response.result.value {
//                print("response", data)
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
}

























