//
//  CallController.swift
//  TestApp
//
//  Created by Zhexenov Samat on 8/18/17.
//  Copyright © 2017 Zhexenov Samat. All rights reserved.
//

import UIKit
import CallKit

class CallController: UIViewController {
    
    let descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 0
        label.textAlignment = .center
//        label.textColor = UIColor.red
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let callButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Звонок в call-centre", for: .normal)
        button.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let indicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.activityIndicatorViewStyle = .whiteLarge
        view.color = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.hidesWhenStopped = true
        return view
    }()
    
    var id = -1
    var callObserver: CXCallObserver!
    var seconds = 0, startSecond = 0
    var number: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        callObserver = CXCallObserver()
        callObserver.setDelegate(self, queue: nil)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        
        view.addSubview(callButton)
        view.addSubview(descLabel)
        view.addSubview(indicator)
        
        NSLayoutConstraint.activate([
            callButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20),
            callButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40),
            callButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40),
            callButton.heightAnchor.constraint(equalToConstant: 60)
            ])
        
        
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.topAnchor.constraint(equalTo: callButton.bottomAnchor, constant: 20).isActive = true
        
        descLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        descLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30).isActive = true
        descLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        descLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        
        callButton.addTarget(self, action: #selector(call), for: .touchUpInside)
   
    }
    
    @objc func call() {
        indicator.startAnimating()
        RequestService.call(completionHandler: {
            resp in
            self.indicator.stopAnimating()
            
            if let response = resp {
                if response.status ?? false {
                    if let id = response.content?.id {
                        self.id = id
                        self.descLabel.text = ""
                        self.makeCall()
                    }
                }
            }
        })
    }
    
    func makeCall(){
        guard let number = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(number)
    }
    
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    
    func putData() {
        if id == -1 {
            return
        }
        print("call duration", seconds)
        indicator.startAnimating()
        RequestService.putCall(id: id, duration: seconds, completionHandler: {
            resp in
            self.indicator.stopAnimating()
            self.descLabel.text = "Call duration: \(self.seconds) sec"
        })
    }
}

extension CallController: CXCallObserverDelegate {
    
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        if call.hasEnded == true {
            seconds = ((Date().millisecondsSince1970 - startSecond) / 1000)
            putData()
            print("Disconnected")
        }
        if call.isOutgoing == true && call.hasConnected == false {
            print("Dialing")
        }
        if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
            print("Incoming")
        }
        if call.hasConnected == true && call.hasEnded == false {
            startSecond = Date().millisecondsSince1970
            print("Connected")
        }
    }
}



















